const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default emails => {
  const invalidEmails =
    emails &&
    emails
      .split(",")
      .map(s => s.trim())
      .filter(Boolean)
      .filter(email => !re.test(email));

  if (invalidEmails && invalidEmails.length) {
    return invalidEmails;
  }
  return;
};
