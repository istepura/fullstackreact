export default [
  {
    label: "Survey Title",
    name: "title",
    noValueError: "You must specify the title!"
  },
  {
    label: "Subject Line",
    name: "subject",
    noValueError: "You must specify the subject!"
  },
  {
    label: "Email Body",
    name: "body",
    noValueError: "You must specify the body!"
  },
  {
    label: "Recipient List",
    name: "recipients",
    noValueError: "You must specify list of recipients"
  }
];
