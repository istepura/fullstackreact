import React from "react";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({ component: Component, auth, ...otherProps }) => (
  <Route
    {...otherProps}
    component={props => {
      if (auth) {
        return <Component {...props} />;
      } else {
        return (
          <Redirect
            to={{
              pathname: "/",
              state: { from: props.location }
            }}
          />
        );
      }
    }}
  />
);

export default PrivateRoute;
