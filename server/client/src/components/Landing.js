import React from "react";

const Landing = () => {
  return (
    <div style={{ textAlign: "center" }} className="z-depth-3">
      <h1>Emaily!</h1>
      Collect feedback from your users
      <div>
        <a href="https://gitlab.com/istepura/fullstackreact">
          https://gitlab.com/istepura/fullstackreact
        </a>
      </div>
    </div>
  );
};

export default Landing;
