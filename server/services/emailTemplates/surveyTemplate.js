const keys = require("../../config/keys");
const host = keys.callbackHost || "http://localhost:3000/";

module.exports = survey => {
  const hrefUrl = `${host}api/surveys/${survey.id}`;
  return `
  <html>
    <body>
      <div style="text-align: center;">
        <h3>We'd like your input!</h3>
        <p>Please answer following question:</p>
        <p>${survey.body}</p>
        <div>
          <a href="${hrefUrl}/yes">Yes</a>
        </div>
        <div>
          <a href="${hrefUrl}/no">No</a>
        </div>
      </div>
    </body>
  </html>;`;
};
