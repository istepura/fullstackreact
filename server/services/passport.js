const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const mongoose = require("mongoose");
const keys = require("../config/keys");
const debug = require("debug")("passport");

const User = mongoose.model("User");

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id).then(user => {
    done(null, user);
  });
});

const cbUrl = "auth/google/callback";
const cbRoot = keys.callbackHost ? keys.callbackHost : "/";
passport.use(
  new GoogleStrategy(
    {
      clientID: keys.googleClientID,
      clientSecret: keys.googleClientSecret,
      callbackURL: cbRoot + cbUrl
    },
    async (accessToken, refreshToken, profile, done) => {
      let user = await User.findOne({ googleId: profile.id });

      debug(`Got user ${user}`);
      if (!user) {
        user = await new User({ googleId: profile.id }).save();
      }
      done(null, user);
    }
  )
);
