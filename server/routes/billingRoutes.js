const keys = require("../config/keys");
const stripe = require("stripe")(keys.stripeSecretKey);
const checkAuth = require("../middlewares/checkAuth");

module.exports = app => {
  app.post("/api/stripe", checkAuth, async (req, res) => {
    const charge = await stripe.charges.create({
      amount: 500,
      currency: "usd",
      description: `Emaily 5 credits for ${req.body.email}`,
      source: req.body.id
    });

    req.user.credits += 5;
    const user = await req.user.save();
    res.send(user);
  });
};
