const mongoose = require("mongoose");
const checkAuth = require("../middlewares/checkAuth");
const checkCredits = require("../middlewares/checkCredits");
const Survey = mongoose.model("Survey");
const Mailer = require("../services/Mailer");
const template = require("../services/emailTemplates/surveyTemplate");
const _ = require("lodash");
const Path = require("path-parser").default;
const { URL } = require("url");
const debugreq = require("debug")("survey-routes:req");
const debugres = require("debug")("survey-routes:res");

const pathParser = new Path("/api/surveys/:surveyId/:choice");
module.exports = app => {
  app.get("/api/surveys/:surveyId/:choice", (req, res) => {
    res.send("Thanks for your feedback!");
  });

  app.get("/api/surveys", checkAuth, async (req, res) => {
    const surveys = await Survey.aggregate()
      .match({
        _user: mongoose.Types.ObjectId(req.user.id)
      })
      .addFields({
        recipientCount: { $size: "$recipients" }
      })
      .project("-recipients")
      .sort("-dateSent")
      .exec();

    res.send(surveys);
  });

  app.post("/api/surveys", checkAuth, checkCredits, async (req, res) => {
    debugreq(req.body);
    const { title, subject, body, recipients } = req.body;

    const survey = new Survey({
      title,
      body,
      subject,
      recipients: recipients.split(",").map(email => ({ email: email.trim() })),
      _user: req.user.id,
      dateSent: Date.now()
    });

    const mailer = new Mailer(survey, template(survey));

    try {
      await mailer.dispatch();
      await survey.save();

      req.user.credits -= 1;
      const user = await req.user.save();

      res.send(user);
    } catch (err) {
      res.status(422).send(err);
    }
  });

  app.post("/api/surveys/webhooks", (req, res) => {
    debugreq(req.body);
    const events = _.chain(req.body)
      .map(({ url, email }) => {
        const match = pathParser.test(new URL(url).pathname);
        if (match) {
          return { email, ...match };
        }
      })
      .compact()
      .uniqBy("email", "surveyId")
      .value();

    debugres(events);

    _.each(events, ({ surveyId, email, choice }) => {
      const choice_parm = choice === "yes" ? "positive" : "negative";
      Survey.updateOne(
        {
          _id: surveyId,
          recipients: {
            $elemMatch: { email: email, responded: false }
          }
        },
        {
          $inc: { [choice_parm]: 1 },
          $set: { "recipients.$.responded": true },
          lastResponded: Date.now()
        }
      ).exec();
    });

    res.send({});
  });
};
