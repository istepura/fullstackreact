const passport = require("passport");

module.exports = app => {
  app.get("/auth/google", (req, res) => {
    passport.authenticate("google", {
      scope: ["profile", "email"],
      state: req.query.returnto
    })(req, res);
  });

  app.get(
    "/auth/google/callback",
    passport.authenticate("google"),
    (req, res) => {
      res.redirect(req.query.state);
    }
  );

  app.get("/api/logout", (req, res) => {
    req.logout();
    res.redirect("/");
  });

  app.get("/api/current_user", (req, res) => {
    res.send(req.user);
  });
};
