const express = require("express");
const mongoose = require("mongoose");
const keys = require("./config/keys");
const session = require("express-session");
const MemoryStore = require("memorystore")(session);
const passport = require("passport");
const bodyParser = require("body-parser");

require("./models/User");
require("./models/Survey");
require("./services/passport");

mongoose.connect(
  keys.mongoURI,
  { useNewUrlParser: true }
);

const app = express();
app.use(bodyParser.json());
app.use(
  session({
    store: new MemoryStore({
      checkPeriod: 24 * 60 * 60 * 1000
    }),
    cookie: { maxAge: 24 * 60 * 60 * 1000 },
    secret: keys.cookieKey,
    resave: false,
    saveUninitialized: false
  })
);
app.use(passport.initialize());
app.use(passport.session());

require("./routes/authRoutes")(app);
require("./routes/billingRoutes")(app);
require("./routes/surveyRoutes")(app);

if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));

  const path = require("path");
  app.get("*", (_req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}
const PORT = process.env.PORT || 5000;
app.listen(PORT);
