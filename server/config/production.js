module.exports = {
  googleClientID: process.env.GOOGLE_CLIENT_ID,
  googleClientSecret: process.env.GOOGLE_CILENT_SECRET,
  mongoURI: process.env.MONGO_URI,
  cookieKey: process.env.COOKIE_KEY,
  callbackHost: process.env.APP_URL,
  stripePublicKey: process.env.STRIPE_PUB_KEY,
  stripeSecretKey: process.env.STRIPE_SEC_KEY,
  sendGridKey: process.env.SEND_GRID_KEY
};
